# DB

## db env 

SQLALCHEMY_DATABASE_URI=sqlite:///todo.db
SQLALCHEMY_TRACK_MODIFICATIONS=False

https://flask-sqlalchemy-russian.readthedocs.io/ru/latest/config.html

## db create

python:
```
from app import db
db.create_all()
exit()
```

## db check

sqlite3:
```
sqlite3 todo.db
.tables
.exit
```

# Route

1. Create user with POST
    - go to POST: http://127.0.0.1:5000/user
    - send POST with JSON data like : {"name": "Admin", "password": "12345"}
    
2. Get ALL users with GET
    - go to GET: http://127.0.0.1:5000/user
    - output is a list of users
    ```json
    {
        "users": [
            {
                "admin": false,
                "name": "Admin",
                "password": "sha256$KIE0FAlF$cd6a7449dadb203e5903abd0d6a6c0b4af88f4e2798ade4bedd712a66eceb0e1",
                "public_id": "32ecc843-1e1f-429a-90ea-a6ba03352533"
            }
        ]
    }
   ```

3. Get ONE user with GET (by id)
    - go to GET:http://127.0.0.1:5000/user/1 >>> "message": "No user found!"
    - go to GET: http://127.0.0.1:5000/user/32ecc843-1e1f-429a-90ea-a6ba03352533
    >>> "Admin" user data