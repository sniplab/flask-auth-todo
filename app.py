import os
from datetime import datetime, timedelta
from functools import wraps

import jwt
from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
import uuid
from werkzeug.security import generate_password_hash, check_password_hash
# do hash for password!


app = Flask(__name__)

app.config['SECRET_KEY'] = os.environ['TOKEN_SECRET_KEY']
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['SQLALCHEMY_DATABASE_URI']  # 'sqlite://todo.db'

db = SQLAlchemy(app)

# Tables:
# user
# todo


class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	public_id = db.Column(db.String(50), unique=True)
	name = db.Column(db.String(50), unique=True)
	password = db.Column(db.String(80))
	admin = db.Column(db.Boolean)


class Todo(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer)
	text = db.Column(db.String(50))
	complete = db.Column(db.Boolean)


# Lets create a decorator
# checking this token is valid/auth token for this user or not
def token_required(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		token = None

		# checking header with key 'x-access-token'
		if 'x-access-token' in request.headers:
			token = request.headers['x-access-token']

		if not token:
			return jsonify({'message': 'Token is missing!'}), 401

		# is valid?
		try:
			data = jwt.decode(token, app.config['SECRET_KEY'], algorithms='HS256')  #algorithms='HS256')
			current_user = User.query.filter_by(public_id=data['public_id']).first()
			# unique, so first
		except:
			return jsonify({'message': 'Token is invalid!'}), 401

		return f(current_user, *args, **kwargs)
	return decorated


# 1. input fields: user_name, password
@app.route('/user', methods=['POST'])
@token_required
def create_user(current_user):
	data = request.get_json()

	# 1. hash a password
	hashed_password = generate_password_hash(data['password'], method='sha256')

	# 2. create a new user in db
	new_user = User(
		public_id=str(uuid.uuid4()),
		name=data['name'],
		password=hashed_password,
		admin=False,  # True/False
	)
	db.session.add(new_user)
	db.session.commit()
	return jsonify({'message': 'New user created!'})


# 2. return a list of all users
@app.route('/user', methods=['GET'])
@token_required
def get_all_users(current_user):

	if not current_user.admin:
		return jsonify({'message': 'Admin right is required!'})

	users = User.query.all()

	output = []
	for user in users:
		user_data = {}
		user_data['public_id'] = user.public_id
		user_data['name'] = user.name
		user_data['password'] = user.password
		user_data['admin'] = user.admin
		output.append(user_data)
	return jsonify({'users': output})


# 3. get info about one user
@app.route('/user/<public_id>', methods=['GET'])
@token_required
def get_one_user(current_user, public_id):

	if not current_user.admin:
		return jsonify({'message': 'Admin right is required!'})

	user = User.query.filter_by(public_id=public_id).first()

	if not user:
		return jsonify({'message': 'No user found!'})

	user_data = {}
	user_data['public_id'] = user.public_id
	user_data['name'] = user.name
	user_data['password'] = user.password
	user_data['admin'] = user.admin

	return jsonify({'user': user_data})


# 3.
@app.route('/user/<public_id>', methods=['PUT'])
@token_required
def promote_user(current_user, public_id):

	if not current_user.admin:
		return jsonify({'message': 'Admin right is required!'})

	user = User.query.filter_by(public_id=public_id).first()

	if not user:
		return jsonify({'message': 'No user found!'})

	user.admin = True
	db.session.commit()

	return jsonify({'message': 'User has been promoted to Admin!'})


# 4. delete the user
@app.route('/user/<public_id>', methods=['DELETE'])
@token_required
def delete_user(current_user, public_id):

	if not current_user.admin:
		return jsonify({'message': 'Admin right is required!'})

	user = User.query.filter_by(public_id=public_id).first()

	if not user:
		return jsonify({'message': 'No user found!'})

	db.session.delete(user)
	db.session.commit()

	return jsonify({'message': 'The user has been deleted!'})

# **********************************************************************


# 5. LOGIN
# get name, password -> give a token with exp timer
# do not need decorator here!
@app.route('/login')
def login():
	auth = request.authorization

	if not auth or not auth.username or not auth.password:
		return make_response(
			"Could not verify", 401,
			{'WWW-Authenticate': "Basic realm='Login Required'"}
		)

	user = User.query.filter_by(name=auth.username).first()

	if not user:
		make_response(
			"Could not verify", 401,
			{'WWW-Authenticate': "Basic realm='Login Required'"}
		)

	# compare hash user.password and with real input auth.password
	# if match, create a token for the user
	if check_password_hash(user.password, auth.password):
		token = jwt.encode(
			# 1. PAYLOAD
			{
				'public_id': user.public_id,
				'exp': datetime.utcnow() + timedelta(minutes=30),
				# 24 h: standard
			},
			# 2. SECRET KEY
			app.config['SECRET_KEY'],
		)
		return jsonify({'token': token})

	return make_response(
			"Could not verify", 401,
			{'WWW-Authenticate': "Basic realm='Login Required'"}
		)

# **********************************************************************

# TODO:

# 1. get all todos
# 2. get one todo
# 3. create todo
# 4. update todo
# 4. delete todo
# add @token_required to all todo routes, and current_user


@app.route('/todo', methods=['POST'])
@token_required
def create_todo(current_user):
	data = request.get_json()

	new_todo = Todo(user_id=current_user.id, text=data['text'], complete=False)
	db.session.add(new_todo)
	db.session.commit()
	return jsonify({'message': 'Todo Created!'})


@app.route('/todo', methods=['GET'])
@token_required
def get_all_todos(current_user):
	# get all todos of current user
	all_todos = Todo.query.filter_by(user_id=current_user.id)
	output = []
	for todo in all_todos:
		user_todo = {}
		user_todo['id'] = todo.id
		user_todo['text'] = todo.text
		user_todo['complete'] = todo.complete
		output.append(user_todo)
	return jsonify({'todos': output})


@app.route('/todo/<todo_id>', methods=['GET'])
@token_required
def get_one_todo(current_user, todo_id):

	todo = Todo.query.filter_by(user_id=current_user.id, id=todo_id).first()

	if not todo:
		return jsonify({'message': 'You have no todo with this id!'})
	return jsonify({
		'id': todo.id,
		'text': todo.text,
		'complete': todo.complete,
	})


@app.route('/todo/<todo_id>', methods=['PUT'])
@token_required
def complete_todo(current_user, todo_id):

	todo = Todo.query.filter_by(user_id=current_user.id, id=todo_id).first()
	todo.complete = True
	db.session.commit()
	return jsonify({'message': 'Todo was Completed!'})


@app.route('/todo/<todo_id>', methods=['DELETE'])
@token_required
def delete_todo(current_user, todo_id):
	todo = Todo.query.filter_by(id=todo_id).first()

	if not todo:
		return jsonify({'message': 'No todo found!'})

	db.session.delete(todo)
	db.session.commit()
	return jsonify({'message': 'Todo was successfully deleted!'})


if __name__ == '__main__':
	app.run(debug=True)
